package com.drahzek.mvcexercise.controller;

import org.springframework.web.bind.annotation.*;

@org.springframework.stereotype.Controller
public class Controller {
    @RequestMapping(value={"/", "/nowe-mapowanie"}, method= RequestMethod.GET)
    public String getHome() {
        return "home.html";
    }

    @RequestMapping(value="/find/{tekst}", method= RequestMethod.GET)
    @ResponseBody
    public String getHello(
        @PathVariable("tekst") String id) {
        return "Witaj " + id + "!!!";
    }

    @GetMapping("/findByQuery")
    @ResponseBody
    public String findByQuery(@RequestParam("name") String name){
        return "Witaj " + name;
    }
}
