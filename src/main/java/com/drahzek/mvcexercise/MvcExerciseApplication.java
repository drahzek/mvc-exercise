package com.drahzek.mvcexercise;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.embedded.EmbeddedServletContainerFactory;
import org.springframework.boot.context.embedded.tomcat.TomcatEmbeddedServletContainerFactory;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MvcExerciseApplication {

	public static void main(String[] args) {
		SpringApplication.run(MvcExerciseApplication.class, args);
	}

}

